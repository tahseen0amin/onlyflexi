package taazuh.com.onlyflexi.Classes;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taazuh.com.onlyflexi.Classes.ApiClient.APIInterface;
import taazuh.com.onlyflexi.Classes.ApiClient.APIManager;
import taazuh.com.onlyflexi.Classes.ApiClient.AppHelper;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.LoginResponse;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.LoginUser;
import taazuh.com.onlyflexi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class LoginFragment extends Fragment {

    TextView tv_username;
    TextView tv_password;
    Button btn_login;

    LoginUser userToLogin ;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootview =  inflater.inflate(R.layout.fragment_login, container, false);


        tv_username = rootview.findViewById(R.id.usernameTextfield);
        tv_password = rootview.findViewById(R.id.passwordTextfield);
        btn_login = rootview.findViewById(R.id.loginButton);

        btn_login.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (verifyTextfields()){
                    String username = tv_username.getText().toString();
                    String password = tv_password.getText().toString();
                    userToLogin = new LoginUser(username, password);
                    loginUser(userToLogin);
                }
            }
        });

        return rootview;
    }

    private Boolean verifyTextfields(){
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        if (!tv_username.getText().toString().isEmpty() && !tv_password.getText().toString().isEmpty()) {
            return true;
        }
        Toast.makeText(getActivity().getApplicationContext(), "Please fill out the details", Toast.LENGTH_SHORT).show();
        return false;
    }

    private void loginUser(final LoginUser userToLogin){
        final APIInterface apiInterface = APIManager.shared().create(APIInterface.class);
        Call<LoginResponse> doLogin = apiInterface.doLogin(userToLogin);

        doLogin.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse response1 = response.body();
                userToLogin.otp_token = response1.otpToken;
                AppHelper.getInstance().user = userToLogin;
                ((MainActivity) getActivity()).onSuccessLogin();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });
    }

}
