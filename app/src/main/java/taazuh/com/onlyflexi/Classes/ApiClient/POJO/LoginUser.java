package taazuh.com.onlyflexi.Classes.ApiClient.POJO;


import com.google.gson.annotations.SerializedName;

/**
 * Created by tahseen0amin on 01/02/2018.
 */

public class LoginUser {

    @SerializedName("username")
    public String username;

    @SerializedName("password")
    public String password;

    @SerializedName("code")
    public String code;

    @SerializedName("otp_token")
    public String otp_token;

    public LoginUser(String username, String password) {
        this.username = username;
        this.password = password;
    }
}
