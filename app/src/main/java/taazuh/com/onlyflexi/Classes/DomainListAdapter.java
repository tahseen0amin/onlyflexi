package taazuh.com.onlyflexi.Classes;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import taazuh.com.onlyflexi.R;


/**
 * Created by tasinzarkoob on 04/02/2018.
 */

class ViewHolder extends RecyclerView.ViewHolder{

    public TextView titleView;

    public ViewHolder(View itemView) {
        super(itemView);
        titleView = (TextView) itemView.findViewById(R.id.titleLabel);
    }
}

public class DomainListAdapter extends RecyclerView.Adapter <ViewHolder> {

    public String[] data;

    public DomainListAdapter(String[] mData){
        this.data = mData;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.domain_list_card_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        holder.titleView.setText(this.data[position]);
    }

    @Override
    public int getItemCount() {
        return this.data.length;
    }

}

