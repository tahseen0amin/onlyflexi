package taazuh.com.onlyflexi.Classes.ApiClient.POJO;

import com.google.gson.annotations.SerializedName;

/**
 * Created by tahseen0amin on 01/02/2018.
 */

public class LoginResponse {

    @SerializedName("challenge")
    public String challenge;

    @SerializedName("otp_token")
    public String otpToken;

    @SerializedName("code")
    public String code;

    @SerializedName("token")
    public String token;


}
