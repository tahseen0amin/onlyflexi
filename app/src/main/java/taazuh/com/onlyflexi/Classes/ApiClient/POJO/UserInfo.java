
package taazuh.com.onlyflexi.Classes.ApiClient.POJO;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfo {

    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("balance")
    @Expose
    private Integer balance;
    @SerializedName("balanceLimit")
    @Expose
    private Integer balanceLimit;
    @SerializedName("services")
    @Expose
    private List<Service> services = null;

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getBalance() {
        return balance;
    }

    public void setBalance(Integer balance) {
        this.balance = balance;
    }

    public Integer getBalanceLimit() {
        return balanceLimit;
    }

    public void setBalanceLimit(Integer balanceLimit) {
        this.balanceLimit = balanceLimit;
    }

    public List<Service> getServices() {
        return services;
    }

    public void setServices(List<Service> services) {
        this.services = services;
    }

}
