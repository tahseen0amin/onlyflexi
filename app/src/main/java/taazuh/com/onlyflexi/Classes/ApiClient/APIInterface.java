package taazuh.com.onlyflexi.Classes.ApiClient;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.DomainList;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.LoginResponse;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.LoginUser;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.UserInfo;

/**
 * Created by tahseen0amin on 01/02/2018.
 */

public interface APIInterface {


    @POST("/account/token")
    Call<LoginResponse> doLogin(@Body LoginUser loginUser);


    @GET("https://ezzerecharge.com/domain/domain_list.json")
    Call<DomainList> getDomainList();

    @GET("/account/info")
    Call<UserInfo> getUserInfo(@Header("apiKey") String apiKey);


    
}
