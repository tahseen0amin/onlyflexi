package taazuh.com.onlyflexi.Classes;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taazuh.com.onlyflexi.Classes.ApiClient.APIInterface;
import taazuh.com.onlyflexi.Classes.ApiClient.APIManager;
import taazuh.com.onlyflexi.Classes.ApiClient.AppHelper;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.LoginResponse;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.LoginUser;
import taazuh.com.onlyflexi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class VerifyPinFragment extends Fragment {

    TextView tv_pin;
    Button btn_verifyPin;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_verify_pin, container, false);

        tv_pin = rootView.findViewById(R.id.pinTextfield);
        btn_verifyPin = rootView.findViewById(R.id.verifyPinButton);

        btn_verifyPin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (verifyTextfields()){
                    String pin = tv_pin.getText().toString();
                    AppHelper.getInstance().user.code = pin;
                    AppHelper.getInstance().user.password = null;
                    verifyToken(AppHelper.getInstance().user);

                }
            }
        });

        return rootView;
    }

    private Boolean verifyTextfields(){
        final InputMethodManager imm = (InputMethodManager) getActivity().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getView().getWindowToken(), 0);
        if (!tv_pin.getText().toString().isEmpty()) {
            return true;
        }
        Toast.makeText(getActivity().getApplicationContext(), "Please fill out the details", Toast.LENGTH_SHORT).show();
        return false;
    }

    private void verifyToken(LoginUser userToLogin){
        final APIInterface apiInterface = APIManager.shared().create(APIInterface.class);
        Call<LoginResponse> doLogin1 = apiInterface.doLogin(userToLogin);
        doLogin1.enqueue(new Callback<LoginResponse>() {
            @Override
            public void onResponse(Call<LoginResponse> call, Response<LoginResponse> response) {
                LoginResponse response1 = response.body();
                SharedPreferences.Editor editor = getActivity().getApplicationContext().getSharedPreferences(AppHelper.PREFS_NAME, 0).edit();
                editor.putString(AppHelper.API_KEY, response1.token);
                editor.apply();
                ((MainActivity) getActivity()).onVerifyingPin();
            }

            @Override
            public void onFailure(Call<LoginResponse> call, Throwable t) {
                Log.e("EROR", t.getMessage());
            }
        });
    }

}
