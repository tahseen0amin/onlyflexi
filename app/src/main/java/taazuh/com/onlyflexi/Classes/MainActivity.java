package taazuh.com.onlyflexi.Classes;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import taazuh.com.onlyflexi.Classes.ApiClient.APIInterface;
import taazuh.com.onlyflexi.Classes.ApiClient.APIManager;
import taazuh.com.onlyflexi.Classes.ApiClient.AppHelper;
import taazuh.com.onlyflexi.R;

public class MainActivity extends AppCompatActivity {

    private TextView headerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        headerView = findViewById(R.id.headerView);

        FragmentManager fm = getFragmentManager();
        FragmentTransaction ft = fm.beginTransaction();

        DomainListFragment dm = new DomainListFragment();
        ft.add(R.id.fragmentContainer, dm, dm.getTag());
        ft.addToBackStack(null);
        ft.commit();

    }

    public void onDomainSelected(String domainName){
        headerView.setText("Domain Selected :->  "+ domainName);
        SharedPreferences.Editor editor = getSharedPreferences(AppHelper.PREFS_NAME, 0).edit();
        editor.putString(AppHelper.DOMAIN_NAME, domainName);
        editor.apply();

        LoginFragment fragment = new LoginFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, fragment, fragment.getTag());
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.animator.slide_up,
                R.animator.slide_down,
                R.animator.slide_up,
                R.animator.slide_down);
        ft.commit();
    }

    public void onSuccessLogin(){
        VerifyPinFragment fragment = new VerifyPinFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, fragment, fragment.getTag());
        ft.addToBackStack(null);
        ft.setCustomAnimations(R.animator.slide_up,
                R.animator.slide_down,
                R.animator.slide_up,
                R.animator.slide_down);
        ft.commit();
    }

    public void onVerifyingPin(){
        Intent goToHome = new Intent(this, MainNavigationActivity.class);
        startActivity(goToHome);
        DomainListFragment fragment = new DomainListFragment();
        FragmentTransaction ft = getFragmentManager().beginTransaction();
        ft.replace(R.id.fragmentContainer, fragment, fragment.getTag());
        ft.commit();
    }


    public static interface ClickListener{
        public void onClick(View view,int position);
        public void onLongClick(View view, int position);
    }

}


