package taazuh.com.onlyflexi.Classes.ApiClient;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import okhttp3.logging.HttpLoggingInterceptor;

/**
 * Created by tahseen0amin on 01/02/2018.
 */

public class APIManager {

    private static Retrofit retrofit = null;

    public static String domainUrl = "https://ezzetest.tk";
   // private static String domainListUri = "https://ezzerecharge.com/domain/domain_list.json";

    public static Retrofit shared() {

        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        OkHttpClient client = new OkHttpClient.Builder().addInterceptor(interceptor).build();

        retrofit = new Retrofit.Builder()
                .baseUrl(domainUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build();

        return retrofit;
    }

}


