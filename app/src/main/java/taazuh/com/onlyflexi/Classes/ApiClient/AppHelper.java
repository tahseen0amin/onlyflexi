package taazuh.com.onlyflexi.Classes.ApiClient;

import android.content.SharedPreferences;

import taazuh.com.onlyflexi.Classes.ApiClient.POJO.LoginUser;

/**
 * Created by tasinzarkoob on 04/02/2018.
 */

public class AppHelper {
    private static final AppHelper ourInstance = new AppHelper();

    public static final String PREFS_NAME = "OnlyFlexiPrefs";
    public static final String API_KEY = "apiKey";
    public static final String DOMAIN_NAME = "domainName";

    public static AppHelper getInstance() {
        return ourInstance;
    }

    private AppHelper() {
    }

    public LoginUser user;



}
