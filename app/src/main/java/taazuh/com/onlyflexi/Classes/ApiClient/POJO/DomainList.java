package taazuh.com.onlyflexi.Classes.ApiClient.POJO;

/**
 * Created by tasinzarkoob on 03/02/2018.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DomainList {

    @SerializedName("domainList")
    @Expose
    private String domainList;

    public String getDomainList() {
        return domainList;
    }

    public void setDomainList(String domainList) {
        this.domainList = domainList;
    }

}