package taazuh.com.onlyflexi.Classes;


import android.content.Context;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.GestureDetector;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import taazuh.com.onlyflexi.Classes.ApiClient.APIInterface;
import taazuh.com.onlyflexi.Classes.ApiClient.APIManager;
import taazuh.com.onlyflexi.Classes.ApiClient.POJO.DomainList;
import taazuh.com.onlyflexi.R;

/**
 * A simple {@link Fragment} subclass.
 */
public class DomainListFragment extends Fragment {

    private static String TAG = "DomainList Fragment";
    private RecyclerView recyclerView;
    private String[] domainList;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_domain_list, container, false);

        recyclerView = rootView.findViewById(R.id.dlRecyclerView);
        recyclerView.setHasFixedSize(true);

        final DomainListAdapter adapter = new DomainListAdapter(new String[0]);
        recyclerView.setAdapter(adapter);

        LinearLayoutManager lm = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(lm);

        final APIInterface apiInterface = APIManager.shared().create(APIInterface.class);
        Call<DomainList> call = apiInterface.getDomainList();

        call.enqueue(new Callback<DomainList>() {
            @Override
            public void onResponse(Call<DomainList> call, Response<DomainList> response) {
                DomainList listresponse = response.body();
                domainList = listresponse.getDomainList().split(",");
                adapter.data = domainList;
                adapter.notifyDataSetChanged();
            }

            @Override
            public void onFailure(Call<DomainList> call, Throwable t) {
                Toast.makeText(getActivity().getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getActivity().getApplicationContext(),
                recyclerView, new MainActivity.ClickListener() {
            @Override
            public void onClick(View view, final int position) {
                ((MainActivity) getActivity()).onDomainSelected(domainList[position]);
            }

            @Override
            public void onLongClick(View view, int position) {
            }
        }));

        return rootView;
    }

}

class RecyclerTouchListener implements RecyclerView.OnItemTouchListener{

    private MainActivity.ClickListener clicklistener;
    private GestureDetector gestureDetector;

    public RecyclerTouchListener(Context context, final RecyclerView recycleView, final MainActivity.ClickListener clicklistener){

        this.clicklistener=clicklistener;
        gestureDetector=new GestureDetector(context,new GestureDetector.SimpleOnGestureListener(){
            @Override
            public boolean onSingleTapUp(MotionEvent e) {
                return true;
            }

            @Override
            public void onLongPress(MotionEvent e) {
                View child=recycleView.findChildViewUnder(e.getX(),e.getY());
                if(child!=null && clicklistener!=null){
                    clicklistener.onLongClick(child,recycleView.getChildAdapterPosition(child));
                }
            }
        });
    }

    @Override
    public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {
        View child=rv.findChildViewUnder(e.getX(),e.getY());
        if(child!=null && clicklistener!=null && gestureDetector.onTouchEvent(e)){
            clicklistener.onClick(child,rv.getChildAdapterPosition(child));
        }

        return false;
    }

    @Override
    public void onTouchEvent(RecyclerView rv, MotionEvent e) {

    }

    @Override
    public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

    }
}